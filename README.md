# a atlasboard module to display News from RSS Providers
Multiple RSS urls can be defined and will be mixed into the display.

The Display will be sliding on the atlasboard. (animation is running every 10 seconds for 1 second)

Example Configuration & Options

Dashboard Layout Config:

      "widgets" : [
          {"row" : 1, "col" : 1, "width" : 3, "height" : 5, "widget" : "rssshow",       "job" : "rssshow",             "config": "rssshowconfig"}
      ]

Rss Widget/Job Configuration:

     "rssshowconfig": {
        "interval" : 600000,  # Interval in milliseconds for refreshing the rss Feeds
         "layout" : "vertical", # vertical and horizontal is supported, adapt the layout accordingly
         "horimagesize" : 100,  # if horizontal layout is used, define the image height, its not used in vertical layout
         "vertimagesize" : 200, # if vertical layout is used, define the image width, its not used in horizontal layout
        "widgetTitle" : "News", # Title for the Widget. (is not displayed)
        "links" : [             #  Various Links for RSS Feeds
		        {"url" : "http://www.diepresse.com/rss/home", "title" : "diepresse.com" }
        ],
        "limit" : 4             # Amount of objects to use from each RSS Feed.
     },

dont copy the comments into the dashboard config.
