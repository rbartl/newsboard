widget = {
    running: false,
    //runs when we receive data from the job
    onData: function (el, data) {

        this.log("rssshow - received data");
        //The parameters our job passed through are in the data object
        //el is our widget element, so our actions should all be relative to that
        if (data.title) {
            $('h2', el).text(data.title);
        }

        $('.rsscontainer', el).empty();
        data.news.forEach(function (item) {
            var template = $('#template-' + data.layout, el).html();
            Mustache.parse(template);
            var rendered = Mustache.render(template, item);
            $('.rsscontainer', el).append(rendered);
        });

        if (data.layout == "horizontal") {
            $('.rsscontainer', el).addClass('rsscontainerHorizontal');
            if (data.horimagesize) {
                $('.rsscontainer img', el).height(data.horimagesize);
            }
            atta
        } else {
            if (data.vertimagesize) {
                $('.rsscontainer img', el).width(data.vertimagesize);
            }
        }

        if (!this.running) {
            var currentmargintop = 0;
            var sliderstep = $('.rsscontainer div:first', el).height();


            window.setInterval(function () {
                var cssfield = "margin-top";
                var animateparams = {};
                if (data.layout == "horizontal") {
                    sliderstep = $('.rsscontainer div:first', el).outerWidth(true);
                    currentmargintop = currentmargintop - sliderstep;
                    cssfield = "margin-left";
                    animateparams = { "margin-left": currentmargintop };
                } else {
                    sliderstep = $('.rsscontainer div:first', el).outerHeight(true);
                    currentmargintop = currentmargintop - sliderstep;
                    animateparams = { "margin-top": currentmargintop };
                }

                $('.rsscontainer', el).animate(animateparams, {
                    duration: 1000,
                    complete: function () {
                        $('.rsscontainer div:first', el).detach().appendTo($('.rsscontainer', el));
                        currentmargintop = 0;
                        $('.rsscontainer', el).css(cssfield, 0);
                    }
                });
            }, 10000);
        }
        this.running = true;


    }
};
