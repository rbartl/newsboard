/**
 * Job: rssshow
 *
 * Expected configuration:
 *
 * { }
 */

/*jslint unparam: true*/
module.exports = function (config, dependencies, job_callback) {
    "use strict";
    var FeedParser = require('feedparser'), request = require('request'), feedMeta;
    var sleep = require('sleep');
    var logger = dependencies.logger;
    var newsparts = {};
    var allnews = [];

    function Rsscontainer(link, linksi, merger) {
        var news = [];
        var idx = 0;
        this.rssworker = function rssworker(res) {
            res.pipe(new FeedParser({}))
                .on('error', function (error) {
                    console.error("failed to fetch RSS Feed " + link.url + ":" + error);
                })
                .on('meta', function (meta) {
                    // Store the metadata for later use
                    feedMeta = meta;
                })
                .on('readable', function () {
                    var stream = this, item = null, ep;
                    while ((item = stream.read()) !== null) {
                        idx++;
                        // Each 'readable' event will contain 1 article
                        // Add the article to the list of episodes
                        ep = {
                            'title': item.title,
                            'mediaUrl': item.link,
                            'publicationDate': item.pubDate,
                            'idx': idx
                        };
                        if (item.enclosures) {
                            item.enclosures.forEach(function (encl) {
                                if (encl.url) {
                                    ep.imageUrl = encl.url;
                                }
                            });
                        }
                        news.push(ep);
                    }
                })
                .on('end', function () {
                    var result = {
                        'feedName': feedMeta.title,
                        'website': feedMeta.link,
                        'albumArt': {
                            'url': feedMeta.image.url
                        },
                        'news': news
                    };
                    merger(linksi, news);
                    // job_callback(null, {title: config.widgetTitle, news: news, layout: config.layout, horimagesize: config.horimagesize});
                });

        }
        var req = request(link);
        this.rssworker(req);
        // https.get(link.url, this.rssworker);
    }

    function newsmerger(linksi, newsarray) {
        newsparts[linksi] = newsarray;
        if (Object.keys(newsparts).length == config.links.length) {
            for (var i = 0; i < config.limit; i++) {
                for (var s = 0; s < Object.keys(newsparts).length; s++) {
                    var newspart = newsparts[s];
                    if (newspart.length > i) {
                        allnews.push(newspart[i]);
                    }
                }

            }
            console.log("callback with news" + allnews);
            job_callback(null, {title: config.widgetTitle, news: allnews, layout: config.layout, horimagesize: config.horimagesize, vertimagesize: config.vertimagesize});
        }
    }

    for (var linksi = 0; linksi < config.links.length; linksi++) {
        var link = config.links[linksi];
        var rsscontainer = new Rsscontainer(link, linksi, newsmerger);
    }


};
